<?php
	session_start();
	ob_start();
	header("Cache-Control: no cache");
  include_once('../connection.php');

  if(!isset($_SESSION['id'])){
    header('Location: ../index.php');
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once('headers.php')?>
</head>

<body id="page-top">

  <div id="wrapper">    

    <?php include_once('sidebar.php')?>

    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

      <?php include_once('topbar.php')?>

        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Add Contact Groups</h1>
          </div>

            <form action="addGroup.php" method="POST">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Group Details</h6>
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-4">
                                <div class="form-group row">
                                    <label for="branch" class="col-3 col-form-label">Group Name</label>
                                    <div class="col-9">
                                        <input class="form-control" type="text" id="groupname" name="groupname" value="<?php echo (isset($_GET['groupname'])) ? ucwords($_GET['groupname']) : "";?>">
                                        <input type="text" hidden id="id" name="id" value="<?php echo (isset($_GET['id'])) ? $_GET['id'] : "";?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="form-group row">
                                    <label for="branch" class="col-2 col-form-label">Members</label>
                                    <div class="col-10">
                                      <div class="card">
                                        <div class="card-body">
                                        <?php
                                          $userquery = "SELECT * FROM users WHERE user_id != 1";
                                          $userresult = $con->query($userquery);
                                          $output = "";

                                          $checkedUsers = (isset($_GET['users'])) ? explode(',',$_GET['users']) : [];
                                          
                                          if ($userresult->num_rows > 0) {
                                              
                                              $count = 0;

                                              while($row = $userresult->fetch_assoc()) {

                                                $id = $row['user_id'];
                                                $name = $row['lastname'].", ".$row['firstname'];

                                                $output = $output."<div class='form-check form-check-inline'>
                                                                <input class='form-check-input' type='checkbox' name='user[$count]' id='user[$count]' value='$id'";

                                                if ( in_array($id,$checkedUsers) ) {

                                                  $output = $output."checked";
                                                }

                                                $output = $output."><small class='form-check-label' for='user[$count]'>$name</small>
                                                            </div>";

                                                $count++;
                                              }

                                              echo $output;
                                          }
                                        ?>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-danger float-right">Save</button>
                    </div>
                </div>
            </form>
        </div>

      </div>

      <?php include_once('footer.php')?>

    </div>
  </div>
  
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include_once('../logoutModal.php'); include_once('endscripts.php')?>

</body>

</html>
