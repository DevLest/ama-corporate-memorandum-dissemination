<?php
    session_start();
    ob_start();
    header("Cache-Control: no cache");
    include_once('../connection.php');
    
    $title = $_GET['title'];
    $status = intval($_GET['status']);
    $start = $_GET['start'];
    $end = $_GET['end'];

    $sqlInsert = "INSERT INTO tbl_events (title,status,start,end) VALUES ('$title','$status','$start','$end')";

    if (!$con->query($sqlInsert)) {
        echo $sqlInsert."\n";
        echo $con->error;
    } else {

        $usersquery = "SELECT * FROM users WHERE user_id != 1";
        $resultusers = $con->query($usersquery);
        $listusers = [];
        
        if ($resultusers->num_rows > 0) {
            
            while($row = $resultusers->fetch_assoc()) {
                array_push( $listusers, $row['user_id'] );
            }
        }

        $noticationquery = "INSERT INTO notifications (title,body,users) VALUES ('New Event Added to the calendar','".strtoupper($title)."','".implode(',',$listusers)."')";
        
        if (!$con->query($noticationquery)) {
            echo $noticationquery."\n";
            echo $con->error;
        
        } else {
            echo true;
        }
    }

    $con->close();

?>