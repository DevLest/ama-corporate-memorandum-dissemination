<?php
	session_start();
	ob_start();
	header("Cache-Control: no cache");
  include_once('../connection.php');

  if(!isset($_SESSION['id'])){
    header('Location: ../index.php');
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once('headers.php')?>
</head>

<body id="page-top">

  <div id="wrapper">    

    <?php include_once('sidebar.php')?>

    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

      <?php include_once('topbar.php')?>

        <div class="container-fluid">
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Welcome <b><?php echo ucwords($_SESSION['lastname'].", ".$_SESSION['firstname']) ?></b></h1>
            </div>

            <form action="addUser.php" method="POST" enctype="multipart/form-data">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Manage Account</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-4">
                                <div class="text-center">
                                    ​<picture>
                                        <img src="data:image/jpeg;base64,<?php echo base64_encode($_SESSION['image']) ?>" class="img-fluid img-thumbnail" alt="user image">
                                        <input type="file" name="image" id="image">
                                    </picture>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="form-group row">
                                    <label for="branch" class="col-3 col-form-label">First Name</label>
                                    <div class="col-9">
                                        <input class="form-control" type="text" placeholder="Firstname" id="fname" name="fname" value="<?php echo $_SESSION['firstname']?>" required>
                                        <input type="text" id="user_id" name="user_id" value="<?php echo $_SESSION['id'] ?>" hidden>
                                        <input type="text" id="type" name="type" value="1" hidden>
                                        <input type="text" id="url_target" name="url_target" value="manage-account.php" hidden>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="branch" class="col-3 col-form-label">Last Name</label>
                                    <div class="col-9">
                                        <input class="form-control" type="text" placeholder="Lastname" id="lname" name="lname" value="<?php echo $_SESSION['lastname']?>" required>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group row">
                                    <label for="branch" class="col-3 col-form-label">Change Password</label>
                                    <div class="col-9">
                                        <input class="form-control" type="password" placeholder="**********" id="password" name="password" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-danger float-right">Save</button>
                    </div>
                </div>
            </form>
        </div>

      </div>

      <?php include_once('footer.php')?>

    </div>
  </div>
  
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Modal -->
  <div class="modal fade" id="memoModal" tabindex="-1" role="dialog" aria-labelledby="memoModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="memoModalTitle">Create Memo</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" name="form1" action="post-memo.php" onsubmit="loadVal();">

          <input type="text" name="sender" value="<?php echo $_SESSION['id']?>" hidden>
          <div class="modal-body">

            <div class="row">

              <div class="col-6">
                <div class="form-group row" style="margin: 2px;">
                    <label for="example-text-input" class="col-4 col-form-label">Memo Type</label>
                    <div class="col-8">
                        <select class="form-control" id="memo_type" name="memo_type" required>
                            <option></option>
                            <option value="1">Question and Answers</option>
                            <option value="2">Announcements</option>
                        </select>
                    </div>
                </div>
              </div>

              <div class="col-6">
                <div class="form-group row" style="margin: 2px;">
                    <label for="example-text-input" class="col-4 col-form-label">Priority Level</label>
                    <div class="col-8">
                        <select class="form-control" id="level" name="level" required>
                            <option></option>
                            <option value="1" class="text-center bg-danger text-white">Critical</option>
                            <option value="2" class="text-center bg-warning text-dark">High</option>
                            <option value="3" class="text-center bg-info text-white">Medium</option>
                            <option value="4" class="text-center bg-success text-white">Low</option>
                        </select>
                    </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-6"></div>
              <div class="col-6">
                <div class="form-group row" style="margin: 2px;">
                  <label for="example-text-input" class="col-4 col-form-label">Expiration Date</label> 
                  <div class="col-8">
                      <input type='date' name="expiration" id="expiration" aria-describedby="expirationHelp" class="form-control" />
                      <small id="expirationHelp" class="form-text text-muted">Until 12 midnight of the expiration time</small>
                  </div>
                </div>
              </div>
            </div>

            <br>
            <div class="form-group row" style="margin: 2px;">
              <label for="example-text-input" class="col-2 col-form-label">Subject</label> 
              <div class="col-10">
                  <input type='text' name="subject" id="subject" class="form-control" />
              </div>
            </div>

            <br>

            <div class="container-fluid" id="ammounement-tab">
              <div id="alerts"></div>
              <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
                <div class="btn-group">
                  <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                    </ul>
                  </div>
                <div class="btn-group">
                  <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                    <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
                    <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
                    <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
                    </ul>
                </div>
                <div class="btn-group">
                  <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                  <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                  <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                  <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                </div>
                <div class="btn-group">
                  <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                  <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                  <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-outdent"></i></a>
                  <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                </div>
                <div class="btn-group">
                  <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                  <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                  <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                  <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                </div>
                <div class="btn-group">
                <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
                  <div class="dropdown-menu input-append">
                    <input class="span2" placeholder="URL" type="text" data-edit="createLink"/>
                    <button class="btn" type="button">Add</button>
                  </div>
                  <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>

                </div>
                
                <div class="btn-group">
                  <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="fa fa-image"></i></a>
                  <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
                </div>
                <div class="btn-group">
                  <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                  <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-redo"></i></a>
                </div>
                <input type="text" data-edit="inserttext" id="voiceBtn" x-webkit-speech="">
              </div>

              <div id="editor"></div>
                  
              <textarea rows="2" name="desc" cols="20" style="display:none;" ></textarea>
            </div>

            <br>
            <div class="row">

              <div class="col-4">

                <div class="form-group row" style="margin: 2px;">
                    <label for="example-text-input" class="col-4 col-form-label">Send to</label>
                    <div class="col-8">
                        <select class="form-control" id="sendto" name="sendto" required>
                            <option></option>
                            <option value="1">Groups</option>
                            <option value="2">Individual Users</option>
                        </select>
                    </div>
                </div>
              </div>

              <div class="col-8">

                <div class="card">
                  <div class="card-body" id="sendtolist">
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Send Memo</button>
          </div>
          
        </form>
      </div>
    </div>
  </div>

  <?php include_once('../logoutModal.php'); include_once('endscripts.php')?>

  <script>
    $(function(){

      function initToolbarBootstrapBindings() {

        var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier', 
              'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
              'Times New Roman', 'Verdana'],
              fontTarget = $('[title=Font]').siblings('.dropdown-menu');

        $.each(fonts, function (idx, fontName) {
            fontTarget.append($('<li><a data-edit="fontName ' + fontName +'" style="font-family:\''+ fontName +'\'">'+fontName + '</a></li>'));
        });

        $('a[title]').tooltip({container:'body'});

        $('.dropdown-menu input').click(function() {
            return false;
          })
          .change(function () {
            $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
          })
          .keydown('esc', function () {this.value='';$(this).change();});

        $('[data-role=magic-overlay]').each(function () { 
          var overlay = $(this), target = $(overlay.data('target')); 
          overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
        });
        
        if ("onwebkitspeechchange"  in document.createElement("input")) {
          var editorOffset = $('#editor').offset();
          $('#voiceBtn').css('position','absolute').offset({top: editorOffset.top, left: editorOffset.left+$('#editor').innerWidth()-35});
        } else {
          $('#voiceBtn').hide();
        }
      };

      function showErrorAlert (reason, detail) {

        var msg='';

        if (reason==='unsupported-file-type') { 
          msg = "Unsupported format " +detail; 
        } else {
          console.log("error uploading file", reason, detail);
        }

        $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+ 
        '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
      };

      initToolbarBootstrapBindings();  

      $('#editor').wysiwyg({ fileUploadError: showErrorAlert} );

      window.prettyPrint && prettyPrint();
      
      var dtToday = new Date();

      var month = dtToday.getMonth() + 1;
      var day = dtToday.getDate();
      var year = dtToday.getFullYear();

      var maxDate = year + '-' + month + '-' + day;
      $('#expiration').attr('min', maxDate);
      $('#expiration').val(maxDate);

      $('#sendto').change(function(){
        $.ajax({
            url: 'getRecipients.php',
            data: {
              id : $('#sendto').val(),
            },
            dataType: "json",
            success: function(data){
              $('#sendtolist').html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr+" | "+thrownError);
            }
        });
      });
    });
  </script>
</body>

</html>
