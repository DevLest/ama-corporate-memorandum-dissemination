<?php
	session_start();
	ob_start();
	header("Cache-Control: no cache");
  include_once('../connection.php');

  if(!isset($_SESSION['id'])){
    header('Location: ../index.php');
  }

  function getName($id, $conn){
    
    $userquery = "SELECT * FROM users WHERE user_id = $id"; 
    $uservalue = mysqli_query($conn, $userquery);
    $userdata = mysqli_fetch_assoc($uservalue);

    return $userdata['lastname'].", ".$userdata['firstname'];
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once('headers.php')?>

  <style>
    #calendar {
        width: 900px;
        margin: 0 auto;
    }

    .response {
        height: 60px;
        width: 100%;
        text-align:center;
    }

    .success {
        background: #cdf3cd;
        padding: 10px 60px;
        border: #c3e6c3 1px solid;
        display: inline-block;
    }

    .fc-content{
      color: white;
      text-align: center;
      padding: 10px 10px;
    }
    </style>
    
    <style>
    .content{
      width: 80%;
      margin: 0 auto;
      margin-top: 50px;
    }


    #editor {
      max-height: 250px;
      height: 250px;
      background-color: white;
      border-collapse: separate; 
      border: 1px solid rgb(204, 204, 204); 
      padding: 10px; 
      box-sizing: content-box; 
      -webkit-box-shadow: rgba(0, 0, 0, 0.0745098) 0px 1px 1px 0px inset; 
      box-shadow: rgba(0, 0, 0, 0.0745098) 0px 1px 1px 0px inset;
      border-top-right-radius: 3px; border-bottom-right-radius: 3px;
      border-bottom-left-radius: 3px; border-top-left-radius: 3px;
      overflow: auto;
      outline: none;
    }

    #voiceBtn {
      width: 20px;
      color: transparent;
      background-color: transparent;
      transform: scale(2.0, 2.0);
      -webkit-transform: scale(2.0, 2.0);
      -moz-transform: scale(2.0, 2.0);
      border: transparent;
      cursor: pointer;
      box-shadow: none;
      -webkit-box-shadow: none;
    }

    div[data-role="editor-toolbar"] {
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    .dropdown-menu a {
      cursor: pointer;
    }
  </style>

  
  <script language="javascript">
    function loadVal(){
      desc = $("#editor").html();
      document.form1.desc.value = desc;
    }
  </script>
</head>

<body id="page-top">

  <div id="wrapper">    

    <?php include_once('sidebar.php')?>

    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

      <?php include_once('topbar.php')?>

        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Inbox</h1>
            <a href="#" data-toggle="modal" data-target="#createmail" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm"><i class="fas fa-envelope fa-sm text-white-50"></i>&nbsp; Create Mail</a>
          </div>

          <?php
            if(isset($_SESSION['DeletionError'])){
              echo "<div style='text-align: center; color:red'>".$_SESSION['DeletionError']."</div>";;
              unset($_SESSION['DeletionError']);
            }
          ?>
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Inbox Details</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>From</th>
                      <th>Subject</th>
                      <th>Date</th>
                      <th>Options</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>From</th>
                      <th>Subject</th>
                      <th>Date</th>
                      <th>Options</th>
                    </tr>
                  </tfoot>
                    <?php 
                      $query = "SELECT * FROM mailbox WHERE reply_for = 0";
                      $values = mysqli_query($con, $query);
                      if (mysqli_num_rows($values) > 0){
                        while ( $mail = mysqli_fetch_assoc( $values ) ) {

                          $is_read = explode(',',$mail['is_read']);
                          
                          echo "
                            <tr onclick='window.location.href  = \"view-mail.php?id=".$mail['id']."\"' class='";

                          if ( !in_array($_SESSION['id'],$is_read) ) echo "bg-primary text-white";
                          
                          $repliesquery = "SELECT * FROM mailbox WHERE reply_for = ".$mail['id'];
                          $repliesvalue = mysqli_query($con, $repliesquery);

                          while ( $row = $repliesvalue->fetch_assoc() ) {
                            
                            $is_readreply = array_values(explode(',',$row['is_read']));

                            if ( !in_array($_SESSION['id'],$is_readreply) ) echo "bg-primary text-white";
                          }

                          echo "'>
                              <td>".getName($mail['sender'],$con)."</td>
                                <td>".$mail['subject']."</td>
                                <td>".date('D, M d,Y',strtotime($mail['date']))."</td>
                                <td>";
                        
                          if ($_SESSION['role'] != 3) {
                            echo   "<a href='delete.php?status=6&id=".$mail['id']."' class='btn btn-danger btn-circle btn-sm'>
                                      <i class='fas fa-trash'></i>
                                    </a>";
                          }
                          echo "</td>
                            </tr>";
                        }
                      }
                      $con->close();
                    ?>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

         <!-- Modal -->
         <div class="modal fade" id="createmail" tabindex="-1" role="dialog" aria-labelledby="createmailtitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="createmailtitle">Create Mail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form method="POST" name="form1" action="send-mail.php" enctype="multipart/form-data" onsubmit="loadVal();">

                <input type="text" name="user" value="<?php echo $_SESSION['id']?>" hidden>
                <div class="modal-body">

                  <div class="container-fluid" id="ammounement-tab">

                    <div class="row">

                      <div class="col-4">

                        <div class="form-group row" style="margin: 2px;">
                            <label for="example-text-input" class="col-4 col-form-label">Send to</label>
                            <div class="col-8">
                                <select class="form-control" id="sendto" name="sendto" required>
                                    <option></option>
                                    <!-- <option value="1">Groups</option> -->
                                    <option value="2">Individual Users</option>
                                </select>
                            </div>
                        </div>
                      </div>

                      <div class="col-8">

                        <div class="card">
                          <div class="card-body" id="sendtolist">
                          </div>
                        </div>
                      </div>
                    </div>

                    <br>
                    <div class="form-group row" style="margin: 2px;">
                      <label for="example-text-input" class="col-2 col-form-label">Subject</label> 
                      <div class="col-10">
                          <input type='text' name="subject" id="subject" class="form-control" />
                      </div>
                    </div>
                    
                    <br><hr>
                    <div id="alerts"></div>
                    <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
                      <div class="btn-group">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
                          <ul class="dropdown-menu">
                          </ul>
                        </div>
                      <div class="btn-group">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                          <ul class="dropdown-menu">
                          <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
                          <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
                          <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
                          </ul>
                      </div>
                      <div class="btn-group">
                        <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                        <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                        <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                        <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                      </div>
                      <div class="btn-group">
                        <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                        <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                        <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-outdent"></i></a>
                        <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                      </div>
                      <div class="btn-group">
                        <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                        <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                        <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                        <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                      </div>
                      <div class="btn-group">
                      <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
                        <div class="dropdown-menu input-append">
                          <input class="span2" placeholder="URL" type="text" data-edit="createLink"/>
                          <button class="btn" type="button">Add</button>
                        </div>
                        <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>

                      </div>
                      
                      <div class="btn-group">
                        <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="fa fa-image"></i></a>
                        <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
                      </div>
                      <div class="btn-group">
                        <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                        <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-redo"></i></a>
                      </div>
                      <input type="text" data-edit="inserttext" id="voiceBtn" x-webkit-speech="">
                    </div>

                    <div id="editor"></div>
                        
                    <textarea rows="2" name="desc" cols="20" style="display:none;" ></textarea>
                  </div>

                  <br>
                  
                  <div>
                      <input type="file" id="attachment" name='attachment'>
                  </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Send</button>
              </div>
              </form>
            </div>
          </div>
        </div>

      </div>

      <?php include_once('footer.php')?>

    </div>
  </div>
  
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include_once('../logoutModal.php'); include_once('endscripts.php')?>

  <script>
  
    $(function(){

      function initToolbarBootstrapBindings() {

        var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier', 
              'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
              'Times New Roman', 'Verdana'],
              fontTarget = $('[title=Font]').siblings('.dropdown-menu');

        $.each(fonts, function (idx, fontName) {
            fontTarget.append($('<li><a data-edit="fontName ' + fontName +'" style="font-family:\''+ fontName +'\'">'+fontName + '</a></li>'));
        });

        $('a[title]').tooltip({container:'body'});

        $('.dropdown-menu input').click(function() {
            return false;
          })
          .change(function () {
            $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
          })
          .keydown('esc', function () {this.value='';$(this).change();});

        $('[data-role=magic-overlay]').each(function () { 
          var overlay = $(this), target = $(overlay.data('target')); 
          overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
        });
        
        if ("onwebkitspeechchange"  in document.createElement("input")) {
          var editorOffset = $('#editor').offset();
          $('#voiceBtn').css('position','absolute').offset({top: editorOffset.top, left: editorOffset.left+$('#editor').innerWidth()-35});
        } else {
          $('#voiceBtn').hide();
        }
      };

      function showErrorAlert (reason, detail) {

        var msg='';

        if (reason==='unsupported-file-type') { 
          msg = "Unsupported format " +detail; 
        } else {
          console.log("error uploading file", reason, detail);
        }

        $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+ 
        '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
      };

      initToolbarBootstrapBindings();  

      $('#editor').wysiwyg({ fileUploadError: showErrorAlert} );

      window.prettyPrint && prettyPrint();

      $('#sendto').change(function(){
        $.ajax({
            url: 'getRecipients.php',
            data: {
              id : $('#sendto').val(),
            },
            dataType: "json",
            success: function(data){
              $('#sendtolist').html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr+" | "+thrownError);
            }
        });
      });
    });
  </script>

</body>

</html>
