<?php
	session_start();
	ob_start();
	header("Cache-Control: no cache");
  include_once('../connection.php');

  if(!isset($_SESSION['id'])){
    header('Location: ../index.php');
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once('headers.php')?>
</head>

<body id="page-top">

  <div id="wrapper">    

    <?php include_once('sidebar.php')?>

    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

      <?php include_once('topbar.php')?>

        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Users</h1>
          </div>

          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">User List</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                        <th>Username</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Branch</th>
                        <th>Department</th>
                        <th>Role</th>
                        <th>Options</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>Username</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Branch</th>
                        <th>Department</th>
                        <th>Role</th>
                        <th>Options</th>
                      </tr>
                    </tfoot>
                      <?php 
                        $query = "SELECT * FROM users WHERE username <> 'admin'";
                        $values = mysqli_query($con, $query);
                        if (mysqli_num_rows($values) > 0){
                          while ( $users = mysqli_fetch_assoc( $values ) ) {

                            $rolequery = "SELECT * FROM roles WHERE id = ".$users['roles']; 
                            $rolevalues = mysqli_query($con, $rolequery);
                            $role = strtoupper(mysqli_fetch_assoc($rolevalues)['role']);

                            $branchquery = "SELECT * FROM branch WHERE id = ".$users['branch_id']; 
                            $branchvalues = mysqli_query($con, $branchquery);
                            $branch = strtoupper(mysqli_fetch_assoc($branchvalues)['branch_name']);

                            $departmentquery = "SELECT * FROM department WHERE id = ".$users['department_id']; 
                            $departmentvalues = mysqli_query($con, $departmentquery);
                            $department = strtoupper(mysqli_fetch_assoc($departmentvalues)['department_name']);

                            echo "
                              <tr>
                                  <td>".$users['username']."</td>
                                  <td>".$users['firstname']."</td>
                                  <td>".$users['lastname']."</td>
                                  <td>$branch</td>
                                  <td>$department</td>
                                  <td>$role</td>";
                            
                            if ( $_SESSION['role'] == 1 ){
                              echo "<td>
                                      <a href='add-users.php?id=".$users['user_id']."&username=".$users['username']."&firstname=".$users['firstname']."&lastname=".$users['lastname']."&branch=".$users['branch_id']."&department_id=".$users['department_id']."&roles=".$users['roles']."' class='btn btn-success btn-circle btn-sm'>
                                          <i class='fas fa-edit'></i>
                                      </a>
                                      <a href='delete.php?status=3&id=".$users['user_id']."' class='btn btn-danger btn-circle btn-sm'>
                                        <i class='fas fa-trash'></i>
                                      </a>
                                    </td>";

                            }
                            echo "
                            </tr>";
                          }
                        }
                        $con->close();
                      ?>
                    <tbody>
                    </tbody>
                  </table>
            </div>
          </div>
        </div>

      </div>

      <?php include_once('footer.php')?>

    </div>
  </div>
  
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include_once('../logoutModal.php'); include_once('endscripts.php')?>
  
</body>

</html>
