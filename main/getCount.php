<?php
    session_start();
    ob_start();
    header("Cache-Control: no cache");
    include_once('../connection.php');
    
    $id = $_GET['id'];
    $output = [
        'inbox' => 0,
        'memo' => 0,
    ];

    $queryInbox = "SELECT * FROM mailbox";
    $resultInbox = $con->query($queryInbox);
    while($rowinbox = $resultInbox->fetch_assoc()) {
        
        if ( in_array( $id, explode( ',' ,$rowinbox['recipients'] ) ) && !in_array( $id, explode( ',' ,$rowinbox['is_read'] ) ) ) $output['inbox']++;
    }

    $queryMemo = "SELECT * FROM memo";
    $resultMemo = $con->query($queryMemo);
    while($row = $resultMemo->fetch_assoc()) {
        
        if ( in_array( $id, explode( ',' ,$row['recipients'] ) ) && !in_array( $id, explode( ',' ,$row['is_read'] ) ) ) $output['memo']++;
    }

    $con->close();

    echo json_encode($output);

?>