<?php
	session_start();
	ob_start();
	header("Cache-Control: no cache");
  include_once('../connection.php');

  if(!isset($_SESSION['id'])){
    header('Location: ../index.php');
  }

  $memoId = $_GET['id'];

  $memoquery = "SELECT * FROM memo WHERE id = $memoId"; 
  $memovalue = mysqli_query($con, $memoquery);
  $memodata = mysqli_fetch_assoc($memovalue);
               
  if( $memodata['level'] == 1 ) {

    $type = "Critical";
    $colortype = "bg-danger text-white";

  } else if( $memodata['level'] == 2 ) {

    $type = "High";
    $colortype = "bg-warning text-dark";

  } else if( $memodata['level'] == 3 ) {

    $type = "Medium";
    $colortype = "bg-info text-white";

  } else if( $memodata['level'] == 4 ) {

    $type = "Low";
    $colortype = "bg-success text-white";

  }

  $acknowledge = explode(',',$memodata['acknowledge']);
  if ( !in_array($_SESSION['id'],$acknowledge) ) {
      $acknowledgestatus = 1;
  } else {
    $acknowledgestatus = 0;
  }

  $is_read = explode(',',$memodata['is_read']);

  if ( !in_array($_SESSION['id'],$is_read) ){

    array_push($is_read, $_SESSION['id']);

    $seenMemo = "UPDATE memo SET is_read = '".implode(',',$is_read)."' WHERE id = $memoId";
    
    $con->query($seenMemo);
  }

  function getName($id, $conn){
    
    $userquery = "SELECT * FROM users WHERE user_id = $id"; 
    $uservalue = mysqli_query($conn, $userquery);
    $userdata = mysqli_fetch_assoc($uservalue);

    return $userdata['lastname'].", ".$userdata['firstname'];
  }

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once('headers.php')?>

  <style>
    img {
        max-width: 100%;
        height: auto;
    }
  </style>

  
  <script language="javascript">
    function loadVal(){
      desc = $("#editor").html();
      document.form1.desc.value = desc;
    }
  </script>
</head>

<body id="page-top">

  <div id="wrapper">    

    <?php include_once('sidebar.php')?>

    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

      <?php include_once('topbar.php')?>

        <div class="container-fluid">
        <?php
            if ( strtotime($memodata['expiration']."24:59:59") < strtotime(date('m-d-Y')) ) {
              echo "<div class='bg-danger text-center text-white' style='padding: 10px 10px; border-radius: 10px;'><strong>This memo has expired!</strong><br><small>Acknowledgement is no longer needed or available.</small></div>";
            } else {
              echo "<div class='$colortype text-center' style='padding: 10px 10px; border-radius: 10px;'>Urgency: <strong>$type</strong></div>";
            }
        ?>

            <br>

          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h2 class="h3 mb-0 text-gray-800"><?php echo $memodata['subject']?></h2>
          </div>

          <div class="row">
            <div class="col-8">
                From: &nbsp;&nbsp;&nbsp;&nbsp;<?php echo getName($memodata['sender'], $con);?>
            </div>
            <div class="col-4">
                Expiration: &nbsp;&nbsp;&nbsp;&nbsp; <?php echo date('D, M d,Y', strtotime($memodata['expiration']));?>
                <input type="text" id="acknowledgestatus" value="<?php echo $acknowledgestatus?>" hidden>
            </div>
          </div>
          
          <br>

          <div class="card">
            <div class="card-body">
                <?php echo $memodata['content']?>
            </div>
        </div>
        <br>
        <?php
            if ( $acknowledgestatus == 1 && strtotime($memodata['expiration']."24:59:59") > strtotime(date('m-d-Y')) ) {
                echo '<button class="float-right btn btn-danger" id="acknowledge" data-toggle="modal" data-target="#acknowledgemodal">Acknowledge Memo</button>';
            }
        ?>
        </div>
        
        <!-- Modal -->
        <form method="POST" action="acknowledge-memo.php">
            <div class="modal fade" id="acknowledgemodal" tabindex="-1" role="dialog" aria-labelledby="acknowledgemodalTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="acknowledgemodalTitle">Acknowledge?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">


                        If you agree with the content of this memo please confirm.
                        <input type="text" name="memoid" value="<?php echo $memodata['id']?>" hidden>
                        <br>
                        <?php
                          if ( $memodata['type'] == 1 ) {
                            echo '
                                <div class="form-group row">
                                    <label for="fname" class="col-3 col-form-label">Answer</label>
                                    <div class="col-9">
                                      <textarea rows="2" name="answer" cols="20" required></textarea>
                                    </div>
                                </div>';
                          }
                        ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Yes</button>
                    </div>
                    </div>
                </div>
            </div>
        </form>

      </div>

      <?php include_once('footer.php')?>

    </div>
  </div>
  
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include_once('../logoutModal.php'); include_once('endscripts.php')?>

  <script>
    $(function(){

        if ( $('#acknowledgestatus').val() == "1" ) {
            var x = setInterval(function() {
                $('#acknowledge').trigger('click')
            }, 60000);
        }
    });
  </script>
</body>

</html>
