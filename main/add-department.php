<?php
	session_start();
	ob_start();
	header("Cache-Control: no cache");
  include_once('../connection.php');

  if(!isset($_SESSION['id'])){
    header('Location: ../index.php');
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once('headers.php')?>
</head>

<body id="page-top">

  <div id="wrapper">    

    <?php include_once('sidebar.php')?>

    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

      <?php include_once('topbar.php')?>

        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Add New Department</h1>
          </div>

            <form action="addDepartment.php" method="POST">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Department Details</h6>
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-6">
                                <div class="form-group row">
                                    <label for="branch" class="col-3 col-form-label">Branch</label>
                                    <div class="col-9">
                                        <?php 
                                            $query = "SELECT * FROM branch";
                                            $result = mysqli_query($con, $query);

                                            $branchid = (isset($_GET['branch'])) ? $_GET['branch'] : "";
                                        ?>
                                        <select class="form-control" id="branch" name="branch" oninput="this.className = ''">
                                            <option></option>
                                            <?php
                                                while ( $branch = mysqli_fetch_assoc( $result ) ) {
                                                    $status = ($branchid != "" && $branchid == $branch['id']) ? "selected='selected'" : "";
                                                    echo "<option value='".$branch['id']."'".$status.">".ucwords($branch['branch_name'])."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group row">
                                    <label for="branch" class="col-3 col-form-label">Department Name</label>
                                    <div class="col-9">
                                        <input class="form-control" type="text" placeholder="HR, IT, Accounting . . . " id="department" name="department" value="<?php echo (isset($_GET['department'])) ? ucwords($_GET['department']) : "";?>">
                                        <input type="text" id="department_id" name="department_id" value="<?php echo (isset($_GET['department_id'])) ? $_GET['department_id'] : "";?>" hidden>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-danger float-right">Save</button>
                    </div>
                </div>
            </form>
        </div>

      </div>

      <?php include_once('footer.php')?>

    </div>
  </div>
  
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include_once('../logoutModal.php'); include_once('endscripts.php')?>

</body>

</html>
