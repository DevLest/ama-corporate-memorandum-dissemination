<?php
    session_start();
    ob_start();
    header("Cache-Control: no cache");
    include_once('../connection.php');

    $subject = $_POST['subject'];
    $content = $_POST['desc'];
    $sender = $_POST['user'];
    $reply_for = ( isset($_POST['reply_for']) ) ? $_POST['reply_for'] : "0";
    
    $file_name = "../uploads/";

    if(isset($_FILES['attachment'])){
        $file_name .= $_FILES['attachment']['name'];
        $file_tmp =$_FILES['attachment']['tmp_name'];
        move_uploaded_file($file_tmp,$file_name);
    }

    if ( isset($_POST['recipients']) ) {

        $recipients = explode(',',$_POST['recipients']);

    } else {

        $sendto = intval($_POST['sendto']);
        $send = $_POST['send'];
        
        $recipients = ['1'];
    
        if ( $sendto == 1 ) {
    
            for( $i = 0; $i < count($send); $i++  ) {
    
                $getGroup = "SELECT * FROM groups WHERE id = ".$send[$i];
                $result = $con->query($getGroup);
    
                while($row = $result->fetch_assoc()) {
                    
                    array_push( $recipients, $row['users'] );
                }
            }
    
        } else {
    
            for( $i = 0; $i < count($send); $i++  ) {
                if ( $send[$i] != null || $send[$i] != "" ){
                    array_push( $recipients, $send[$i] );
                }
            }
        }
    }

    $query = "INSERT INTO mailbox(sender,recipients,subject,content,is_read,reply_for,filename) VALUES('$sender','".implode(',',$recipients)."','$subject','$content','','$reply_for','$file_name')";

    if ($con->query($query)){

        $noticationquery = "INSERT INTO notifications (title,body,users) VALUES ('Check it out! You have an EMAIL','".strtoupper($subject)."','".implode(',',$recipients)."')";
        
        if (!$con->query($noticationquery)) {
            echo $noticationquery."\n";
            echo $con->error;
        
        } else {
            // header('Location: inbox.php');
        }
    } else {
        echo $con->error;
    }
    
    $con->close();
?>