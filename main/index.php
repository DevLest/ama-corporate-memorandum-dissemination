<?php
	session_start();
  ob_start();
	header("Cache-Control: no cache");
  include_once('../connection.php');

  if(!isset($_SESSION['id'])){
    header('Location: ../index.php');
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once('headers.php')?>

  <style>
    #calendar {
        width: 900px;
        margin: 0 auto;
    }

    .response {
        height: 60px;
        width: 100%;
        text-align:center;
    }

    .success {
        background: #cdf3cd;
        padding: 10px 60px;
        border: #c3e6c3 1px solid;
        display: inline-block;
    }

    .fc-content{
      color: white;
      text-align: center;
      padding: 10px 10px;
    }
    </style>
    
    <style>
    .content{
      width: 80%;
      margin: 0 auto;
      margin-top: 50px;
    }


    #editor {
      max-height: 250px;
      height: 250px;
      background-color: white;
      border-collapse: separate; 
      border: 1px solid rgb(204, 204, 204); 
      padding: 10px; 
      box-sizing: content-box; 
      -webkit-box-shadow: rgba(0, 0, 0, 0.0745098) 0px 1px 1px 0px inset; 
      box-shadow: rgba(0, 0, 0, 0.0745098) 0px 1px 1px 0px inset;
      border-top-right-radius: 3px; border-bottom-right-radius: 3px;
      border-bottom-left-radius: 3px; border-top-left-radius: 3px;
      overflow: auto;
      outline: none;
    }

    #voiceBtn {
      width: 20px;
      color: transparent;
      background-color: transparent;
      transform: scale(2.0, 2.0);
      -webkit-transform: scale(2.0, 2.0);
      -moz-transform: scale(2.0, 2.0);
      border: transparent;
      cursor: pointer;
      box-shadow: none;
      -webkit-box-shadow: none;
    }

    div[data-role="editor-toolbar"] {
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    .dropdown-menu a {
      cursor: pointer;
    }
  </style>

  
  <script language="javascript">
    function loadVal(){
      desc = $("#editor").html();
      document.form1.desc.value = desc;
    }
  </script>
</head>

<body id="page-top">

  <div id="wrapper">

    <?php include_once('sidebar.php')?>

    <div id="content-wrapper" class="d-flex flex-column">

      <input type="text" id="role" value=<?php echo $_SESSION['role']?> hidden>
      
      <div id="content">

        <?php include_once('topbar.php')?>

        <div class="container-fluid">

          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
          </div>
          
          <div class="row">

            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Memos</div>

                      <?php

                        $role = $_SESSION['role'];
                        $countmemo = 0;

                        $memoQuery = "SELECT * FROM memo";
                        $memoCount = mysqli_query($con, $memoQuery);

                        while($row = $memoCount->fetch_assoc()) {

                          $recipients = explode(',',$row['recipients']);
                          if ( in_array($_SESSION['id'],$recipients) ) $countmemo++;
                        }

                        echo "<div class='mb-0 font-weight-bold text-gray-800' style='font-size:100px;'>$countmemo</div>";
                      ?>

                    </div>
                    <div class="col-auto">
                      <i class="fas fa-file-alt fa-5x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              
                <div class="card-footer bg-transparent border-primary">
                  <a href="memos.php">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  </a>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Employees</div>

                        <?php

                          $branch = $_SESSION['branch'];
                          $employeeQuery = "SELECT * FROM users WHERE branch_id = $branch";
                          $employeeCount = mysqli_query($con, $employeeQuery);

                          echo "<div class='mb-0 font-weight-bold text-gray-800' style='font-size:100px;'>".mysqli_num_rows($employeeCount)."</div>";
                        ?>

                    </div>
                    <div class="col-auto">
                      <i class="fas fa-sitemap fa-5x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              
                <div class="card-footer bg-transparent border-success">
                  <a href="user-list.php">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  </a>
                </div>
              </div>
            </div>

            
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Departments</div>

                        <?php

                          $department = $_SESSION['department'];
                          $role = $_SESSION['role'];

                          if ( $role == 1) {

                            $departmentQuery = "SELECT * FROM department";
                          } else if ( $role == 2 ){
                            
                            $departmentQuery = "SELECT * FROM department WHERE id = $department";
                          } else {
                            
                            $departmentQuery = "SELECT * FROM department WHERE  id = 0";
                          }

                          $departmentCount = mysqli_query($con, $departmentQuery);

                          echo "<div class='mb-0 font-weight-bold text-gray-800' style='font-size:100px;'>".mysqli_num_rows($departmentCount)."</div>";
                        ?>

                    </div>
                    <div class="col-auto">
                      <i class="fas fa-archive fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
                
                <div class="card-footer bg-transparent border-danger">
                  <a href="department-list.php">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  </a>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Branches</div>

                        <?php

                          $branches = $_SESSION['branch'];
                          $role = $_SESSION['role'];

                          if ( $role == 1) {

                            $branchQuery = "SELECT * FROM branch";
                          } else {
                            
                            $branchQuery = "SELECT * FROM branch WHERE id = 0";
                          }

                          $branchCount = mysqli_query($con, $branchQuery);

                          echo "<div class='mb-0 font-weight-bold text-gray-800' style='font-size:100px;'>".mysqli_num_rows($branchCount)."</div>";
                        ?>

                    </div>
                    <div class="col-auto">
                      <i class="fas fa-archive fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              
                <div class="card-footer bg-transparent border-warning">
                  <a href="branch-list.php">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  </a>
                </div>
              </div>
            </div>
          </div>

          <!-- calendar and feeds -->
          <br>
          <hr>
          <div class="row">
              <div class="col-lg-5">
                <h3 class="page-header">News Feed</h3>

                <div class="card" style="height:95%;">
                  <div class="card-body">

                    <?php
                      if ( $_SESSION['role'] == 1 ) {
                        echo '
                        <div class="input-group mb-3">
                          <input type="text" class="form-control" placeholder="What\'s on your mind?" data-toggle="modal" data-target="#feedModal" aria-label="Recipient\'s username" aria-describedby="basic-addon2">
                          <div class="input-group-append">
                            <button class="float-right btn btn-danger" data-toggle="modal" data-target="#feedModal">Post</button>
                          </div>
                        </div>';
                      }
                      
                    ?>
                    <hr>

                    <?php
                      $feedsquery = "SELECT * FROM feeds";
                      $feedvalue = $con->query($feedsquery);

                      while($row = $feedvalue->fetch_assoc()) {

                        $userquery = "SELECT * FROM users WHERE user_id =".$row['user_id'];
                        $uservalue = $con->query($userquery);
                        $userdata = $uservalue->fetch_assoc();

                        $fullname = $userdata['firstname']." ".$userdata['lastname'];

                        echo "<div class='card shadow mb-4'>
                        
                                <div class='card-header py-3 d-flex flex-row align-items-center justify-content-between'>
                                  <h6 class='m-0 font-weight-bold text-primary'>$fullname</h6>
                                  <div class='dropdown no-arrow'>
                                    <small class='form-check-label' for='name'>".date('D, M d,Y',strtotime($row['created_at']))."</small>";
                        if ( $_SESSION['role'] != 3 && $_SESSION['id'] == $row['user_id'] ) {
                          echo "<a class='dropdown-toggle' href='#' role='button' id='dropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                  <i class='fas fa-ellipsis-v fa-sm fa-fw text-gray-400'></i>
                                </a>
                                <div class='dropdown-menu dropdown-menu-right shadow animated--fade-in' aria-labelledby='dropdownMenuLink' x-placement='bottom-end' style='position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);'>
                                  <a class='dropdown-item' href='delete.php?id=".$row['id']."&status=5'>Delete</a>
                                </div>";
                        }
                        echo      "</div>
                                </div>
                            
                                <div class='card-body'>
                                  ".$row['content']."
                                </div>
                              </div>";
                      }
                    ?>

                  </div>
                </div>
              </div>
              <div class="col-lg-7">
                  <h3 class="page-header">Calendar of Events</h3>
                  <br>
                  <div class="panel panel-default">
                      <div class="panel-heading">
                          <div class="response"></div>
                          <div id='calendar'></div>
                      </div>
                  </div>
              </div>
          </div>

        <!-- Modal -->
        <div class="modal fade" id="feedModal" tabindex="-1" role="dialog" aria-labelledby="feedModalTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="feedModalTitle">Post Feed</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form method="POST" name="form1" action="post-feed.php" onsubmit="loadVal();">

                <input type="text" name="user" value="<?php echo $_SESSION['id']?>" hidden>
                <div class="modal-body">

                  <div class="container-fluid" id="ammounement-tab">
                    <div id="alerts"></div>
                    <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
                      <div class="btn-group">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
                          <ul class="dropdown-menu">
                          </ul>
                        </div>
                      <div class="btn-group">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                          <ul class="dropdown-menu">
                          <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
                          <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
                          <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
                          </ul>
                      </div>
                      <div class="btn-group">
                        <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                        <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                        <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                        <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                      </div>
                      <div class="btn-group">
                        <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                        <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                        <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-outdent"></i></a>
                        <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                      </div>
                      <div class="btn-group">
                        <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                        <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                        <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                        <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                      </div>
                      <div class="btn-group">
                      <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
                        <div class="dropdown-menu input-append">
                          <input class="span2" placeholder="URL" type="text" data-edit="createLink"/>
                          <button class="btn" type="button">Add</button>
                        </div>
                        <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>

                      </div>
                      
                      <div class="btn-group">
                        <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="fa fa-image"></i></a>
                        <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
                      </div>
                      <div class="btn-group">
                        <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                        <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-redo"></i></a>
                      </div>
                      <input type="text" data-edit="inserttext" id="voiceBtn" x-webkit-speech="">
                    </div>

                    <div id="editor"></div>
                        
                    <textarea rows="2" name="desc" cols="20" style="display:none;" ></textarea>
                  </div>

                    <div class="col-8">

                      <div class="card">
                        <div class="card-body" id="sendtolist">
                        </div>
                      </div>
                    </div>
                  </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Post</button>
              </div>
              </form>
            </div>
          </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="calendarInput" tabindex="-1" role="dialog" aria-labelledby="calendarInputTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="calendarInputTitle">Add Event<Title></Title></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <div class="modal-body">
                <div class="form-group row" style="margin: 2px;">
                    <label for="example-text-input" class="col-4 col-form-label">Event Title</label>
                    <div class="col-8">
                      <input type="text" id="eventtitle" name="eventtitle" class="form-control">
                    </div>
                </div>

                <div class="form-group row" style="margin: 2px;">
                    <label for="example-text-input" class="col-4 col-form-label">Priority Level</label>
                    <div class="col-8">
                        <select class="form-control" id="level" name="level" required>
                            <option></option>
                            <option value="1" class="text-center bg-danger text-white">Critical</option>
                            <option value="2" class="text-center bg-warning text-dark">High</option>
                            <option value="3" class="text-center bg-info text-white">Medium</option>
                            <option value="4" class="text-center bg-success text-white">Low</option>
                        </select>
                    </div>
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success" id="postEvent">Post</button>
              </div>
            </div>
          </div>
        </div>

      </div>
      <?php include_once('footer.php')?>

    </div>

  </div>
  
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  
  <?php include_once('../logoutModal.php'); include_once('endscripts.php')?>
  <script>

    var startdate;
    var enddate;
    var alldays;
    var calendar;
  
    $(function(){

      function initToolbarBootstrapBindings() {

        var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier', 
              'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
              'Times New Roman', 'Verdana'],
              fontTarget = $('[title=Font]').siblings('.dropdown-menu');

        $.each(fonts, function (idx, fontName) {
            fontTarget.append($('<li><a data-edit="fontName ' + fontName +'" style="font-family:\''+ fontName +'\'">'+fontName + '</a></li>'));
        });

        $('a[title]').tooltip({container:'body'});

        $('.dropdown-menu input').click(function() {
            return false;
          })
          .change(function () {
            $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
          })
          .keydown('esc', function () {this.value='';$(this).change();});

        $('[data-role=magic-overlay]').each(function () { 
          var overlay = $(this), target = $(overlay.data('target')); 
          overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
        });
        
        if ("onwebkitspeechchange"  in document.createElement("input")) {
          var editorOffset = $('#editor').offset();
          $('#voiceBtn').css('position','absolute').offset({top: editorOffset.top, left: editorOffset.left+$('#editor').innerWidth()-35});
        } else {
          $('#voiceBtn').hide();
        }
      };

      function showErrorAlert (reason, detail) {

        var msg='';

        if (reason==='unsupported-file-type') { 
          msg = "Unsupported format " +detail; 
        } else {
          console.log("error uploading file", reason, detail);
        }

        $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+ 
        '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
      };

      initToolbarBootstrapBindings();  

      $('#editor').wysiwyg({ fileUploadError: showErrorAlert} );

      window.prettyPrint && prettyPrint();

      $('#postEvent').on('click', function(event) {

        $.ajax({
          url: 'add-event.php',
          data: {
            title : $('#eventtitle').val(),
            status : $('#level').val(),
            start : startdate,
            end : enddate,
          },
          dataType: "json",
          success: function (data) {

            if (data){
              displayMessage("Added Successfully");
            } else {
              displayMessage(data);
            }
          }
        });
        calendar.fullCalendar('renderEvent',
          {
            title: $('#eventtitle').val(),
            start: startdate,
            end: enddate,
            allDay: alldays
          },
          true
        );

        $('#calendarInput').modal('hide');
        calendar.fullCalendar('unselect');
      });
    });

    $(document).ready(function () {

      if ( $('#role').val() == 1 ) {
        calendar = $('#calendar').fullCalendar({

          editable: true,
          events: "fetch-event.php",
          displayEventTime: false,

          eventRender: function (event, element, view) {
            if (event.allDay === 'true') {
              event.allDay = true;
            } else {
              event.allDay = false;
            }
          },

          selectable: true,
          selectHelper: true,
          
          select: function (start, end, allDay) {
           startdate = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
           enddate = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
           alldays = allDay;

            $('#calendarInput').modal('show');
            
            // var title = prompt('Event Title:');

            // if (title) {
            //   var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
            //   var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");

            //   $.ajax({
            //     url: 'add-event.php',
            //     data: 'title=' + title + '&start=' + start + '&end=' + end,
            //     type: "POST",
            //     success: function (data) {
            //       displayMessage("Added Successfully");
            //     }
            //   });
            //   calendar.fullCalendar('renderEvent',
            //     {
            //       title: title,
            //       start: start,
            //       end: end,
            //       allDay: allDay
            //     },
            //     true
            //   );
            // }
            // calendar.fullCalendar('unselect');
          },
          
          editable: true,
          eventDrop: function (event, delta) {
            var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
            var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
            $.ajax({
              url: 'edit-event.php',
              data: 'title=' + event.title + '&start=' + start + '&end=' + end + '&id=' + event.id,
              type: "POST",
              success: function (response) {
                displayMessage("Updated Successfully");
              }
            });
          },

          eventClick: function (event) {
            var deleteMsg = confirm("Do you really want to delete?");
            if (deleteMsg) {
              $.ajax({
                type: "POST",
                url: "delete-event.php",
                data: "&id=" + event.id,
                success: function (response) {
                  if(parseInt(response) > 0) {
                    $('#calendar').fullCalendar('removeEvents', event.id);
                    displayMessage("Deleted Successfully");
                  }
                }
              });
            }
          }
        });

      } else {
        
        calendar = $('#calendar').fullCalendar({

          editable: true,
          events: "fetch-event.php",
          displayEventTime: false,

          eventRender: function (event, element, view) {
            if (event.allDay === 'true') {
              event.allDay = true;
            } else {
              event.allDay = false;
            }
          },

          selectable: true,
          selectHelper: true
        });
      }

      
    });

    function displayMessage(message) {
      $(".response").html("<div class='success'>"+message+"</div>");
      setInterval(function() { $(".success").fadeOut(); }, 1000);
    }
  </script>

</body>

</html>
