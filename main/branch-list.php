<?php
	session_start();
	ob_start();
	header("Cache-Control: no cache");
  include_once('../connection.php');

  if(!isset($_SESSION['id'])){
    header('Location: ../index.php');
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once('headers.php')?>
</head>

<body id="page-top">

  <div id="wrapper">    

    <?php include_once('sidebar.php')?>

    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

      <?php include_once('topbar.php')?>

        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Branch</h1>
          </div>

          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Branch List</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                        <th>Branch</th>
                        <th>Options</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                        <th>Branch</th>
                        <th>Options</th>
                        </tr>
                    </tfoot>
                    <tbody>
                      <?php 
                        $query = "SELECT * FROM branch";
                        $values = mysqli_query($con, $query);
                        if (mysqli_num_rows($values) > 0){
                          while ( $branch = mysqli_fetch_assoc( $values ) ) {

                            echo "
                              <tr>
                                    <td>".ucwords($branch['branch_name'])."</td>";
                                    
                            if ( $_SESSION['role'] == 1 ){
                              echo "<td>
                                      <a href='add-branch.php?branch=".$branch['branch_name']."&id=".$branch['id']."' class='btn btn-success btn-circle btn-sm'>
                                      <i class='fas fa-edit'></i>
                                      </a>
                                      <a href='delete.php?status=1&id=".$branch['id']."' class='btn btn-danger btn-circle btn-sm'>
                                      <i class='fas fa-trash'></i>
                                      </a>
                                  </td>";
                            }
                            echo "
                            </tr>";
                          }
                        }
                        $con->close();
                      ?>
                    </tbody>
                </table>
            </div>
          </div>
        </div>

      </div>

      <?php include_once('footer.php')?>

    </div>
  </div>
  
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include_once('../logoutModal.php'); include_once('endscripts.php')?>

</body>

</html>
