<?php
	session_start();
	ob_start();
	header("Cache-Control: no cache");
  include_once('../connection.php');

  if(!isset($_SESSION['id'])){
    header('Location: ../index.php');
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include_once('headers.php')?>
</head>

<body id="page-top">

    <div id="wrapper">    

        <?php include_once('sidebar.php')?>

        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">

                <?php include_once('topbar.php')?>

                <div class="container-fluid">
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Add New Branch</h1>
                    </div>
                    <form action="addBranch.php" method="POST">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Branch Details</h6>
                            </div>
                            <div class="card-body">

                                <div class="form-group row">
                                    <label for="branch" class="col-3 col-form-label">Branch Location / Name</label>
                                    <div class="col-9">
                                        <input class="form-control" type="text" placeholder="Bacolod" id="branch" name="branch" value="<?php echo (isset($_GET['branch'])) ? $_GET['branch'] : "";?>">
                                        <input type="text"id="id" name="id" value="<?php echo (isset($_GET['id'])) ? $_GET['id'] : "";?>" hidden>
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-danger float-right">Save</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>

            <?php include_once('footer.php')?>
        </div>
    </div>
  
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <?php include_once('../logoutModal.php'); include_once('endscripts.php')?>

</body>

</html>
