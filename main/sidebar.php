<ul class="navbar-nav bg-gradient-danger sidebar sidebar-dark accordion" id="accordionSidebar">

<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
  <div class="sidebar-brand-text mx-3">Commonwealth Insurance</div>
</a>

<hr class="sidebar-divider my-0">

<li class="nav-item <?php if ($_SERVER['REQUEST_URI'] == "/main/index.php") { echo "active"; } else { echo ""; }; ?>">
  <a class="nav-link" href="index.php">
    <i class="fas fa-fw fa-tachometer-alt"></i>
    <span>Dashboard</span></a>
</li>

<hr class="sidebar-divider">

<div class="sidebar-heading">
  Modules
</div>

<li class="nav-item <?php if ($_SERVER['REQUEST_URI'] == "/main/inbox.php") { echo "active"; } else { echo ""; }; ?>">
  <a class="nav-link" href="inbox.php">
    <i class="fas fa-fw fa-inbox"></i>
    <span>Inbox &nbsp;&nbsp;&nbsp;<span class='badge badge-danger bg-danger text-white' style='display:none;' id="inboxcount"></span></span>
  </a>
</li>

<li class="nav-item <?php if ($_SERVER['REQUEST_URI'] == "/main/memos.php") { echo "active"; } else { echo ""; }; ?>">
  <a class="nav-link" href="memos.php">
    <i class="fas fa-fw fa-envelope-open-text"></i>
    <span>Memos &nbsp;&nbsp;&nbsp;<span class='badge badge-danger bg-danger text-white' style='display:none;' id="memocount"></span></span>
  </a>
</li>

<?php
  if ( $_SESSION['role'] != 3 ){
    echo '<li class="nav-item';
    if ($_SERVER['REQUEST_URI'] == "/main/add-branch.php" || $_SERVER['REQUEST_URI'] == "/main/branch-list.php") { echo " active"; } else { echo ""; };
    echo '">
        <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-code-branch"></i>
          <span>Branches</span>
        </a>
        <div id="collapseTwo" class="collapse';
    if ($_SERVER['REQUEST_URI'] == "/main/add-branch.php" || $_SERVER['REQUEST_URI'] == "/main/branch-list.php") { echo " show"; } else { echo ""; }; 
    echo '" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Branch Components:</h6>
            <a class="collapse-item';
            
    if ( $_SESSION['role'] == 1 ){
      if ($_SERVER['REQUEST_URI'] == "/main/add-branch.php") { echo " active"; } else { echo ""; };
      echo '" href="add-branch.php">
                <i class="fas fa-fw fa-plus"></i>
                &nbsp; Add Branch
              </a>
              <a class="collapse-item';
    }
    
    if ($_SERVER['REQUEST_URI'] == "/main/branch-list.php") { echo " active"; } else { echo ""; }; 
    echo '" href="branch-list.php">
              <i class="fas fa-fw fa-list"></i>
              &nbsp; Branch List
            </a>
          </div>
        </div>
      </li>

      <li class="nav-item';
    if ($_SERVER['REQUEST_URI'] == "/main/add-department.php" || $_SERVER['REQUEST_URI'] == "/main/department-list.php") { echo " active"; } else { echo ""; };
    echo '">
        <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          <i class="fas fa-fw fa-building"></i>
          <span>Departments</span>
        </a>
        <div id="collapseOne" class="collapse';
    if ($_SERVER['REQUEST_URI'] == "/main/add-department.php" || $_SERVER['REQUEST_URI'] == "/main/department-list.php") { echo " show"; } else { echo ""; }; 
    echo '" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Department Components:</h6>
            <a class="collapse-item';
            
    if ( $_SESSION['role'] == 1 ){
      if ($_SERVER['REQUEST_URI'] == "/main/add-department.php") { echo " active"; } else { echo ""; }; 
      echo '" href="add-department.php">
                <i class="fas fa-fw fa-plus"></i>
                &nbsp; Add Department
              </a>
              <a class="collapse-item';
    }

    if ($_SERVER['REQUEST_URI'] == "/main/department-list.php") { echo " active"; } else { echo ""; }; 
    echo '" href="department-list.php">
              <i class="fas fa-fw fa-list"></i>
              &nbsp; Department List
            </a>
          </div>
        </div>
      </li>
      
      <li class="nav-item';
    if ($_SERVER['REQUEST_URI'] == "/main/add-users.php" || $_SERVER['REQUEST_URI'] == "/main/user-list.php") { echo " active"; } else { echo ""; }; 
    echo '">
        <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
          <i class="fas fa-fw fa-user"></i>
          <span>Users</span>
        </a>
        <div id="collapseThree" class="collapse';
    if ($_SERVER['REQUEST_URI'] == "/main/add-users.php" || $_SERVER['REQUEST_URI'] == "/main/user-list.php") { echo " show"; } else { echo ""; }; 
    echo '" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Users Components:</h6>
            <a class="collapse-item';
            
    if ( $_SESSION['role'] == 1 ){
      if ($_SERVER['REQUEST_URI'] == "/main/add-users.php") { echo " active"; } else { echo ""; }; 
      echo '" href="add-users.php">
                <i class="fas fa-fw fa-plus"></i>
                &nbsp; Add Users
              </a>
              <a class="collapse-item';
    }
    if ($_SERVER['REQUEST_URI'] == "/main/user-list.php") { echo " active"; } else { echo ""; }; 
    echo '" href="user-list.php">
              <i class="fas fa-fw fa-list"></i>
              &nbsp; Users List
            </a>
          </div>
        </div>
      </li>
      
      <li class="nav-item';
    // if ($_SERVER['REQUEST_URI'] == "/main/group-list.php" || $_SERVER['REQUEST_URI'] == "/main/add-groups.php") { echo " active"; } else { echo ""; }; 
    // echo '">
    //     <a class="nav-link collapsed" href="" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
    //       <i class="fas fa-fw fa-users"></i>
    //       <span>Groups</span>
    //     </a>
    //     <div id="collapseFour" class="collapse';
    // if ($_SERVER['REQUEST_URI'] == "/main/group-list.php" || $_SERVER['REQUEST_URI'] == "/main/add-groups.php") { echo " show"; } else { echo ""; }; 
    // echo '" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    //       <div class="bg-white py-2 collapse-inner rounded">
    //         <h6 class="collapse-header">Group Components:</h6>
    //         <a class="collapse-item';
            
    // if ( $_SESSION['role'] == 1 ){
    //   if ($_SERVER['REQUEST_URI'] == "/main/add-groups.php") { echo " active"; } else { echo ""; }; 
    //   echo '" href="add-groups.php">
    //             <i class="fa fa-address-book"></i>
    //             &nbsp; Add Goups
    //           </a>
    //           <a class="collapse-item';
    // }
    // if ($_SERVER['REQUEST_URI'] == "/main/group-list.php") { echo " active"; } else { echo ""; }; 
    // echo '" href="group-list.php">
    //           <i class="fas fa-fw fa-list"></i>
    //           &nbsp; Group List
    //         </a>
    //       </div>
    //     </div>
    //   </li>';
  } 
?>

<hr class="sidebar-divider d-none d-md-block">

<div class="text-center d-none d-md-inline">
  <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

</ul>