<?php
	session_start();
	ob_start();
	header("Cache-Control: no cache");
  include_once('../connection.php');

  if(!isset($_SESSION['id'])){
    header('Location: ../index.php');
  }

  $memoId = $_GET['id'];

  $memoquery = "SELECT * FROM memo WHERE id = $memoId"; 
  $memovalue = mysqli_query($con, $memoquery);
  $memodata = mysqli_fetch_assoc($memovalue);

  function getName($id, $conn){
    
    $userquery = "SELECT * FROM users WHERE user_id = $id"; 
    $uservalue = mysqli_query($conn, $userquery);
    $userdata = mysqli_fetch_assoc($uservalue);

    return $userdata['lastname'].", ".$userdata['firstname'];
  }

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once('headers.php')?>
</head>

<body id="page-top">

  <div id="wrapper">    

    <?php include_once('sidebar.php')?>

    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

      <?php include_once('topbar.php')?>

        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Memo: <?php echo ucwords($memodata['subject'])?></h1>
          </div>

          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">User Answers</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                        <th>User</th>
                        <th>Answer</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                        <th>User</th>
                        <th>Answer</th>
                        </tr>
                    </tfoot>
                    <tbody>
                      <?php 
                        $query = "SELECT * FROM memo_answers WHERE memo_id = $memoId";
                        $values = $con->query($query);

                        if (mysqli_num_rows($values) > 0){

                          while ( $memo = mysqli_fetch_assoc( $values ) ) {

                            echo "<tr>
                                    <td>".ucwords(getName($memo['user_id'], $con))."</td>
                                    <td>".$memo['answer']."</td>
                                </tr>";
                          }
                        }
                        $con->close();
                      ?>
                    </tbody>
                </table>
            </div>
          </div>
        </div>

      </div>

      <?php include_once('footer.php')?>

    </div>
  </div>
  
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include_once('../logoutModal.php'); include_once('endscripts.php')?>

</body>

</html>
