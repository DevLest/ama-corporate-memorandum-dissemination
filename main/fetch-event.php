<?php
    session_start();
    ob_start();
    header("Cache-Control: no cache");
    include_once('../connection.php');
    include_once('../vendor/autoload.php');

    $json = array();
    $sqlQuery = "SELECT * FROM tbl_events ORDER BY id";
    $lastid = 0;

    $result = mysqli_query($con, $sqlQuery);
    $eventArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        array_push($eventArray, $row);
        $lastid = $row['id'];
    }
    mysqli_free_result($result);

    $key = 'e3ee6c66-4da6-4c0e-b85a-4f060ab8b07e';
    $holiday_api = new \HolidayAPI\Client(['key' => $key]);

    try {

        // Fetch holidays with minimum parameters
        $holidays = $holiday_api->holidays([
            'country' => 'PH',
            'year' => date('Y') - 1,
        ]);

        foreach ( $holidays['holidays'] as $holiday ) {
            $lastid++;

            $eventHoliday['id'] = $lastid;
            $eventHoliday['title'] = $holiday['name'];
            $eventHoliday['status'] = '1';
            $eventHoliday['start'] = $holiday['date'];
            $eventHoliday['end'] = $holiday['date'];
            array_push($eventArray, $eventHoliday);
        }
    } catch (Exception $e) {
        var_dump($e);
    }

    $con->close();
    echo json_encode($eventArray);
?>