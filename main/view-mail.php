<?php
	session_start();
	ob_start();
	header("Cache-Control: no cache");
  include_once('../connection.php');

  if(!isset($_SESSION['id'])){
    header('Location: ../index.php');
  }

  $mailid = $_GET['id'];

  $mailquery = "SELECT * FROM mailbox WHERE id = $mailid"; 
  $mailvalue = mysqli_query($con, $mailquery);
  $maildata = mysqli_fetch_assoc($mailvalue);

  $is_read = array_values(explode(',',$maildata['is_read']));

  if ( !in_array($_SESSION['id'],$is_read) ){

    array_push($is_read, $_SESSION['id']);

    $seenMail = "UPDATE mailbox SET is_read = '".implode(',',$is_read)."' WHERE id = $mailid";
    
    $con->query($seenMail);
  }

  $repliesquery = "SELECT * FROM mailbox WHERE reply_for = ".$maildata['id'];
  $repliesvalue = mysqli_query($con, $repliesquery);

  function getName($id, $conn){
    
    $userquery = "SELECT * FROM users WHERE user_id = $id"; 
    $uservalue = mysqli_query($conn, $userquery);
    $userdata = mysqli_fetch_assoc($uservalue);

    return $userdata['lastname'].", ".$userdata['firstname'];
  }

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once('headers.php')?>

  <style>
    img {
        max-width: 100%;
        height: auto;
    }

    #calendar {
        width: 900px;
        margin: 0 auto;
    }

    .response {
        height: 60px;
        width: 100%;
        text-align:center;
    }

    .success {
        background: #cdf3cd;
        padding: 10px 60px;
        border: #c3e6c3 1px solid;
        display: inline-block;
    }

    .fc-content{
      color: white;
      text-align: center;
      padding: 10px 10px;
    }
    </style>
    
    <style>
    .content{
      width: 80%;
      margin: 0 auto;
      margin-top: 50px;
    }


    #editor {
      max-height: 250px;
      height: 250px;
      background-color: white;
      border-collapse: separate; 
      border: 1px solid rgb(204, 204, 204); 
      padding: 10px; 
      box-sizing: content-box; 
      -webkit-box-shadow: rgba(0, 0, 0, 0.0745098) 0px 1px 1px 0px inset; 
      box-shadow: rgba(0, 0, 0, 0.0745098) 0px 1px 1px 0px inset;
      border-top-right-radius: 3px; border-bottom-right-radius: 3px;
      border-bottom-left-radius: 3px; border-top-left-radius: 3px;
      overflow: auto;
      outline: none;
    }

    #voiceBtn {
      width: 20px;
      color: transparent;
      background-color: transparent;
      transform: scale(2.0, 2.0);
      -webkit-transform: scale(2.0, 2.0);
      -moz-transform: scale(2.0, 2.0);
      border: transparent;
      cursor: pointer;
      box-shadow: none;
      -webkit-box-shadow: none;
    }

    div[data-role="editor-toolbar"] {
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    .dropdown-menu a {
      cursor: pointer;
    }
  </style>

  
  <script language="javascript">
    function loadVal(){
        desc = $("#editor").html();
        document.form1.desc.value = desc;
    }
  </script>
</head>

<body id="page-top">

    <div id="wrapper">    

        <?php include_once('sidebar.php')?>

        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">

                <?php include_once('topbar.php')?>

                <div class="container-fluid">

                    <br>

                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h2 class="h3 mb-0 text-gray-800"><?php echo $maildata['subject']?></h2>
                        <span class="d-none d-sm-inline-block"><?php echo date('D, M d,Y',strtotime($maildata['date']))?></span>
                    </div>

                    <p class="text-left"><small class="text-right">From: &nbsp;&nbsp;&nbsp;&nbsp;<?php echo getName($maildata['sender'], $con);?></small></p>
                    <div class="card">
                        <div class="card-body">
                            <?php echo $maildata['content']?>
                        </div>
                    </div>

                    <?php
                        while ( $row = $repliesvalue->fetch_assoc() ) {
                            
                            $is_readreply = array_values(explode(',',$row['is_read']));

                            if ( !in_array($_SESSION['id'],$is_readreply) ){

                                array_push($is_readreply, $_SESSION['id']);

                                $seenReply = "UPDATE mailbox SET is_read = '".implode(',',$is_readreply)."' WHERE id = ".$row['id'];
                                
                                $con->query($seenReply);
                            }

                            if ( $row['sender'] == $maildata['sender'] ) {
                                $align = "text-left";
                            } else {
                                $align = "text-right";
                            }

                            if ( $row['sender'] == $_SESSION['id'] ) {
                                $color = "bg-info text-white";
                            } else {
                                $color = "";
                            }

                            echo "<br>
                                <p class='$align'><small>From: &nbsp;&nbsp;&nbsp;&nbsp".getName($row['sender'], $con)."</small></p>
                                <div class='card'>
                                    <div class='card-body $align $color'>
                                        ".$row['content']."
                                    </div>
                                </div>";
                        }
                    ?>

                    <br>
                    <button class="float-right btn btn-success" id="reply" data-toggle="modal" data-target="#replyMail">Reply</button>
                </div>
                
                <!-- Modal -->
                <form method="POST" name="form1" action="send-mail.php" onsubmit="loadVal();">
                    <input type="text" name="user" hidden value="<?php echo $_SESSION['id']?>">
                    <input type="text" name="subject" hidden value="<?php echo $maildata['subject']?>">
                    <input type="text" name="recipients" hidden value="<?php echo $maildata['recipients']?>">
                    <input type="text" name="reply_for" hidden value="<?php echo $maildata['id']?>">
                    <div class="modal fade" id="replyMail" tabindex="-1" role="dialog" aria-labelledby="replyMailTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="replyMailTitle">Reply to mail</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div id="alerts"></div>
                                        <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
                                            <div class="btn-group">
                                                <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
                                                <ul class="dropdown-menu">
                                                </ul>
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                                                <ul class="dropdown-menu">
                                                    <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
                                                    <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
                                                    <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
                                                </ul>
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                                                <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                                                <a class="btn" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                                                <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                                                <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                                                <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-outdent"></i></a>
                                                <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                                                <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                                                <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                                                <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
                                                <div class="dropdown-menu input-append">
                                                    <input class="span2" placeholder="URL" type="text" data-edit="createLink"/>
                                                    <button class="btn" type="button">Add</button>
                                                </div>
                                                <a class="btn" data-edit="unlink" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>

                                            </div>
                                            
                                            <div class="btn-group">
                                                <a class="btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="fa fa-image"></i></a>
                                                <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
                                            </div>
                                            <div class="btn-group">
                                                <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                                                <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-redo"></i></a>
                                            </div>
                                            <input type="text" data-edit="inserttext" id="voiceBtn" x-webkit-speech="">
                                        </div>

                                        <div id="editor"></div>
                                            
                                        <textarea rows="2" name="desc" cols="20" style="display:none;" ></textarea>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Send</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <?php include_once('footer.php')?>

            </div>
        </div>
    </div>
  
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include_once('../logoutModal.php'); include_once('endscripts.php')?>

  <script>
    $(function(){

      function initToolbarBootstrapBindings() {

        var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier', 
              'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
              'Times New Roman', 'Verdana'],
              fontTarget = $('[title=Font]').siblings('.dropdown-menu');

        $.each(fonts, function (idx, fontName) {
            fontTarget.append($('<li><a data-edit="fontName ' + fontName +'" style="font-family:\''+ fontName +'\'">'+fontName + '</a></li>'));
        });

        $('a[title]').tooltip({container:'body'});

        $('.dropdown-menu input').click(function() {
            return false;
          })
          .change(function () {
            $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
          })
          .keydown('esc', function () {this.value='';$(this).change();});

        $('[data-role=magic-overlay]').each(function () { 
          var overlay = $(this), target = $(overlay.data('target')); 
          overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
        });
        
        if ("onwebkitspeechchange"  in document.createElement("input")) {
          var editorOffset = $('#editor').offset();
          $('#voiceBtn').css('position','absolute').offset({top: editorOffset.top, left: editorOffset.left+$('#editor').innerWidth()-35});
        } else {
          $('#voiceBtn').hide();
        }
      };

      function showErrorAlert (reason, detail) {

        var msg='';

        if (reason==='unsupported-file-type') { 
          msg = "Unsupported format " +detail; 
        } else {
          console.log("error uploading file", reason, detail);
        }

        $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+ 
        '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
      };

      initToolbarBootstrapBindings();  

      $('#editor').wysiwyg({ fileUploadError: showErrorAlert} );

      window.prettyPrint && prettyPrint();

        if ( $('#acknowledgestatus').val() == "1" ) {
            var x = setInterval(function() {
                $('#acknowledge').trigger('click')
            }, 60000);
        }
    });
  </script>
</body>

</html>
