<?php
	session_start();
	ob_start();
	header("Cache-Control: no cache");
  include_once('../connection.php');

  if(!isset($_SESSION['id'])){
    header('Location: ../index.php');
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php include_once('headers.php')?>
</head>

<body id="page-top">

  <div id="wrapper">    

    <?php include_once('sidebar.php')?>

    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">

      <?php include_once('topbar.php')?>

        <div class="container-fluid">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Add Users</h1>
          </div>

          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Register User</h6>
            </div>
            <div class="card-body">
                <form id="regForm" action="addUser.php" method="POST">
                    <input type="text" id="user_id" name="user_id" value="<?php echo (isset($_GET['id'])) ? $_GET['id'] : "";?>" hidden>

                    <div class="tab">
                        <div class="wizard-heading wizard-heading-md">Account Information</div>
                        <div class="form-group row">
                            <label for="fname" class="col-3 col-form-label">Username</label>
                            <div class="col-9">
                                <input class="form-control" type="text" placeholder="UserMe29" id="username" name="username" oninput="this.className = ''" value="<?php echo (isset($_GET['username'])) ? $_GET['username'] : "";?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-3 col-form-label">Password</label>
                            <div class="col-9">
                                <input class="form-control" type="password" placeholder="******" id="password" name="password" oninput="this.className = ''">
                            </div>
                        </div>
                    </div>

                    <div class="tab">
                        <div class="wizard-heading wizard-heading-md">Basic personal information</div>
                        <div class="form-group row">
                            <label for="fname" class="col-3 col-form-label">First Name</label>
                            <div class="col-9">
                                <input class="form-control" type="text" placeholder="Juan" id="fname" name="fname" oninput="this.className = ''" value="<?php echo (isset($_GET['firstname'])) ? $_GET['firstname'] : "";?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-3 col-form-label">Last Name</label>
                            <div class="col-9">
                                <input class="form-control" type="text" placeholder="Makata" id="lname" name="lname" oninput="this.className = ''" value="<?php echo (isset($_GET['lastname'])) ? $_GET['lastname'] : "";?>">
                            </div>
                        </div>
                    </div>

                    <div class="tab">

                        <div class="wizard-heading wizard-heading-md">Designation</div>

                        <div class="form-group row">
                            <label for="fname" class="col-3 col-form-label">Branch</label>
                            <div class="col-9">
                                <?php 
                                    $query = "SELECT * FROM branch";
                                    $result = mysqli_query($con, $query);
                                    $branchid = (isset($_GET['branch'])) ? $_GET['branch'] : "";
                                ?>
                                <select class="form-control" id="branch" name="branch" oninput="this.className = ''">
                                    <option></option>
                                    <?php
                                        while ( $branch = mysqli_fetch_assoc( $result ) ) {
                                            $status = ($branchid != "" && $branchid == $branch['id']) ? "selected='selected'" : "";
                                            echo "<option value='".$branch['id']."'".$status.">".ucwords($branch['branch_name'])."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fname" class="col-3 col-form-label">Department</label>
                            <div class="col-9">
                                <input type="text" id="department_id" name="department_id" value="<?php echo (isset($_GET['department_id'])) ? $_GET['department_id'] : "0";?>" hidden>
                                <select class="form-control" id="department" name="department" oninput="this.className = ''">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="fname" class="col-3 col-form-label">Role</label>
                            <div class="col-9">
                                <?php 
                                    $query = "SELECT * FROM roles WHERE id != 1";
                                    $result = mysqli_query($con, $query);
                                    $roleid = (isset($_GET['roles'])) ? $_GET['roles'] : "";
                                ?>
                                <select class="form-control" id="role" name="role" oninput="this.className = ''">
                                    <option></option>
                                    <?php
                                        while ( $role = mysqli_fetch_assoc( $result ) ) {
                                            $roles = ($roleid != "" && $roleid == $role['id']) ? "selected='selected'" : "";
                                            echo "<option value='".$role['id']."'".$roles.">".ucwords($role['role'])."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div style="overflow:auto;">
                        <div style="float:right;">
                            <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                            <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
                        </div>
                    </div>

                    <div style="text-align:center;margin-top:40px;">
                        <span class="step"></span>
                        <span class="step"></span>
                        <span class="step"></span>
                    </div>

                </form>
            </div>
          </div>
        </div>

      </div>

      <?php include_once('footer.php')?>

    </div>
  </div>
  
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include_once('../logoutModal.php'); include_once('endscripts.php')?>
  
  <script>

        $( '#branch' ).change(function(){
            $.ajax({
                url: '../api/getDepartment.php',
                data: {
                        id : $('#branch').val(),
                        tag : $('#department_id').val(),
                },
                dataType: "json",
                success: function(data){
                    $('#department').html(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr+" | "+thrownError);
                }
            });
        });

        if ( $('#department_id').val() != "0" ) {
            $.ajax({
                url: '../api/getDepartment.php',
                data: {
                        id : $('#branch').val(),
                        tag : $('#department_id').val(),
                },
                dataType: "json",
                success: function(data){
                    $('#department').html(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr+" | "+thrownError);
                }
            });
        }

        var currentTab = 0; // Current tab is set to be the first tab (0)
        showTab(currentTab); // Display the current tab

        function showTab(n) {
            // This function will display the specified tab of the form ...
            var x = document.getElementsByClassName("tab");
            x[n].style.display = "block";
            // ... and fix the Previous/Next buttons:
            if (n == 0) {
                document.getElementById("prevBtn").style.display = "none";
            } else {
                document.getElementById("prevBtn").style.display = "inline";
            }
            if (n == (x.length - 1)) {
                document.getElementById("nextBtn").innerHTML = "Submit";
            } else {
                document.getElementById("nextBtn").innerHTML = "Next";
            }
            // ... and run a function that displays the correct step indicator:
            fixStepIndicator(n)
        }

        function nextPrev(n) {
            // This function will figure out which tab to display
            var x = document.getElementsByClassName("tab");
            // Exit the function if any field in the current tab is invalid:
            if (n == 1 && !validateForm()) return false;
            // Hide the current tab:
            x[currentTab].style.display = "none";
            // Increase or decrease the current tab by 1:
            currentTab = currentTab + n;
            // if you have reached the end of the form... :
            if (currentTab >= x.length) {
                //...the form gets submitted:
                document.getElementById("regForm").submit();
                return false;
            }
            // Otherwise, display the correct tab:
            showTab(currentTab);
        }

        function validateForm() {
            
            var x, y, i, valid = true;
            x = document.getElementsByClassName("tab");
            y = x[currentTab].getElementsByTagName("input");
            
            for (i = 0; i < y.length; i++) {
                
                console.log(y[i]);
                if (y[i].value == "") {
                    
                    y[i].className += " invalid";
                    valid = false;
                }
            }

            y = x[currentTab].getElementsByTagName("select");

            for (i = 0; i < y.length; i++) {
                
                if (y[i].value == "" || y[i].value == null) {
                    
                    y[i].className += " invalid";
                    valid = false;
                }
            }
            
            if (valid) {
                document.getElementsByClassName("step")[currentTab].className += " finish";
            }
            return valid; // return the valid status
        }

        function fixStepIndicator(n) {
            
            var i, x = document.getElementsByClassName("step");
            for (i = 0; i < x.length; i++) {
                x[i].className = x[i].className.replace(" active", "");
            }
            
            x[n].className += " active";
        }
  </script>

</body>

</html>
