<?php
    session_start();
    ob_start();
    header("Cache-Control: no cache");
    include_once('../connection.php');

    $type = $_GET['id'];
    $output = "";
    $count = 0;

    if ( $type == 1 ){
        $query = "SELECT * FROM groups";
    } else {
        $query = "SELECT * FROM users WHERE user_id != 1";
    }
    
    $result = $con->query($query);

    if ($result->num_rows > 0) {

        while($row = $result->fetch_assoc()) {

            if ( $type == 1 ) {
                $members = count(explode(',',$row['users']));
                $id = $row['id'];
                $name = $row['groupname']." - ".$members."/users";
            } else {
                $id = $row['user_id'];
                $name = $row['lastname'].", ".$row['firstname'];
            }

            $output = $output."<div class='form-check form-check-inline'>
                            <input class='form-check-input' type='checkbox' name='send[$count]' id='send[$count]' value='$id'>
                            <small class='form-check-label' for='send[$count]'>$name</small>
                        </div>";

            $count++;
        }
    } else {
        $output = "";
    }

    echo json_encode($output);
?>