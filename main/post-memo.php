<?php
    session_start();
    ob_start();
    header("Cache-Control: no cache");
    include_once('../connection.php');

    $type = intval($_POST['memo_type']);
    $level = intval($_POST['level']);
    $expiration = date('Y-m-d' ,strtotime($_POST['expiration']));
    $subject = $_POST['subject'];
    $content = $_POST['desc'];
    $sendto = intval($_POST['sendto']);
    $send = $_POST['send'];
    $sender = $_POST['sender'];
    
    $file_name = "../uploads/";

    if(isset($_FILES['attachment'])){
        $file_name .= $_FILES['attachment']['name'];
        $file_tmp =$_FILES['attachment']['tmp_name'];
        move_uploaded_file($file_tmp,$file_name);
    }

    $recipients = ['1'];

    if ( $sendto == 1 ) {

        for( $i = 0; $i < count($send); $i++  ) {

            $getGroup = "SELECT * FROM groups WHERE id = ".$send[$i];
            $result = $con->query($getGroup);

            while($row = $result->fetch_assoc()) {
                
                array_push( $recipients, $row['users'] );
            }
        }

    } else {

        for( $i = 0; $i < count($send); $i++  ) {
            if ( $send[$i] != null || $send[$i] != "" ){
                array_push( $recipients, $send[$i] );
            }
        }
    }

    $query = "INSERT INTO memo(sender,type,level,expiration,subject,content,recipients,acknowledge,is_read,filename) VALUES('$sender','$type','$level','$expiration','$subject','$content','".implode(',',$recipients)."','','','$file_name')";

    if ($con->query($query)){

        $noticationquery = "INSERT INTO notifications (title,body,users) VALUES ('You have a received a MEMO','".strtoupper($subject)."','".implode(',',$recipients)."')";
        
        if (!$con->query($noticationquery)) {
            echo $noticationquery."\n";
            echo $con->error;
        
        } else {
            header('Location: memos.php');
        }
        $con->close();
    } else {
        echo $con->error;
    }
?>