<?php
    session_start();
    ob_start();
    session_destroy();
    $_SESSION = array();
    header('Location: index.php')
?>