<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitc761597fd1095ca995fc293cc5840ae1
{
    public static $prefixLengthsPsr4 = array (
        'H' => 
        array (
            'HolidayAPI\\' => 11,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'HolidayAPI\\' => 
        array (
            0 => __DIR__ . '/..' . '/holidayapi/holidayapi-php/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitc761597fd1095ca995fc293cc5840ae1::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitc761597fd1095ca995fc293cc5840ae1::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
