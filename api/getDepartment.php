<?php
    session_start();
    ob_start();
    header("Cache-Control: no cache");
    include_once('../connection.php');

    $id = $_GET['id'];
    $tag = $_GET['tag'];

    $query = "SELECT * FROM department WHERE branch_id = $id";
    $result = mysqli_query($con, $query);

    $output = "";

    if (mysqli_num_rows($result) > 0) {
        while ( $department = mysqli_fetch_assoc( $result ) ) {
            $status = ($tag != "" && $tag == $department['id']) ? "selected='selected'" : "";
            $output .= "<option value='".$department['id']."'".$status.">".ucwords($department['department_name'])."</option>";
        }
    } else {
        $output .= "<option>No departments available for this Branch</option>";
    }

    echo json_encode($output);
    $con->close();
?>