$(function(){
    
    if (Notification.permission !== "granted") {		
        Notification.requestPermission();
    }

    var x = setInterval(function() {

        $.ajax({
            url: 'getCount.php',
            data: {
              id : $('#sessionuserid').val(),
            },
            dataType: "json",
            success: function(data){
                if (parseInt(data.inbox) != 0){
                    $('#inboxcount').html(data.inbox);
                    $('#inboxcount').attr("style","");
                } else {
                    $('#inboxcount').attr("style","display:none");
                }

                if (parseInt(data.memo) != 0){
                    $('#memocount').html(data.memo);
                    $('#memocount').attr("style","");
                } else {
                    $('#memocount').attr("style","display:none");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr+" | "+thrownError);
            }
        });
    }, 60000);
    

    var y = setInterval(function() {
        

        $.ajax({
            url: 'getNotifications.php',
            data: {
              id : $('#sessionuserid').val(),
            },
            dataType: "json",
            success: function(data){
                for (var i = data.length - 1; i >= 0; i--) {
                    console.log(data[i]['title']);
                    sendNotification(data[i]['title'],data[i]['body'],window.location.href);
                };
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr+" | "+thrownError);
            }
        });
    }, 60000);

});

function sendNotification(title,body,url) {
        
    if (!Notification) {
        $('body').append('<h4 style="color:red">*Browser does not support Web Notification</h4>');
        return;
    }
    if (Notification.permission !== "granted") {		
        Notification.requestPermission();
    } else {	
        var notifikasi = new Notification(title, {
            icon: "../img/logo.jpg",
            body: body,
        });
        notifikasi.onclick = function () {
            window.open(url); 
            notifikasi.close();     
        };
        setTimeout(function(){
            notifikasi.close();
        }, 5000);
    }
}