<?php
	session_start();
	ob_start();
	header("Cache-Control: no cache");
  include_once('connection.php');
  
	if(isset($_SESSION['id'])){
    header($_SESSION['location']);
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="img/logo.jpg">

  <title>Corporate Memorandum Dissemination System</title>

  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="css/sb-admin-2.css" rel="stylesheet">

</head>

<body>

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Welcome to <br>Commonwealth Isurance Company</h1>
                  </div>
                  <div class="text-center">
                  </div>
                  <form class="user" method="POST" action='auth.php'>
                    <div class="form-group">
                      <input type="input" class="form-control form-control-user" id="exampleInputEmail" name='uname' aria-describedby="emailHelp" placeholder="Enter Your Username...">
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" id="exampleInputPassword" name='psw' placeholder="Password">
                    </div>
                    <div class="form-group">
                      <!-- <div class="custom-control custom-checkbox small"> -->
                        <!-- <input type="checkbox" class="custom-control-input" id="customCheck"> -->
                        <!-- <label class="custom-control-label" for="customCheck">Remember Me</label> -->
                      <!-- </div> -->
                    </div>
			              <button class="btn btn-danger btn-user btn-block" type="submit">Login</button>
                  </form>
                  <hr>
                  <div class="text-center">
                    <?php if(isset($_SESSION['msg'])){ echo "<div class='small' style='text-align: center; padding-top: 5px; color: red; font-weight:bold'>".$_SESSION['msg']."</div>";} ?>
                  </div>
                  <div class="text-center">
                    <!-- <a class="small" href="register.html">Create an Account!</a> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>
